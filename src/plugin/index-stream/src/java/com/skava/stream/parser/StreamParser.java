
package com.skava.stream.parser;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.parse.HTMLMetaTags;
import org.apache.nutch.parse.ParseFilter;
import org.apache.nutch.parse.Outlink;
import org.apache.nutch.parse.Parse;
import org.apache.nutch.parse.ParseStatusCodes;
import org.apache.nutch.parse.ParseStatusUtils;
import org.apache.nutch.parse.Parser;
import org.apache.nutch.storage.ParseStatus;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.util.NutchConfiguration;
import org.apache.nutch.util.TableUtil;
import org.apache.oro.text.regex.MatchResult;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.PatternCompiler;
import org.apache.oro.text.regex.PatternMatcher;
import org.apache.oro.text.regex.PatternMatcherInput;
import org.apache.oro.text.regex.Perl5Compiler;
import org.apache.oro.text.regex.Perl5Matcher;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class StreamParser implements Parser {

    public static final Logger LOG = LoggerFactory.getLogger(StreamParser.class);

    private Configuration conf;

    @Override
    public Parse getParse(String url, WebPage page) {

        System.out.println("In stream parser");

        String parts[] = url.split("/");


        LOG.info("In stream parser+++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println(parts);

        String content = new String(page.getContent().array());
        Vector<Outlink> outlinks = new Vector<Outlink>();

        // convert to json...
        JSONParser parser = new JSONParser();
        String name = "Title";
        try {
            Object obj = parser.parse(content);
            JSONObject json = (JSONObject)obj;

            String type = (String) json.get("type");
            System.out.println("TYPE: " + type);

            name = (String) json.get("name");
            name = name.split("|")[0];

            if (type.equals("category") || type.equals("sub_category")) {
                System.out.println("IN PARSER CATEGORY");
                outlinks = getOutlinksFromCategory("http://llbstage.skavaone.com/skavastream/core/v1/llbean/", json, outlinks);
            }

            if (type.equals("productList") || type.equals("LLB_productList")) {
                System.out.println("IN PARSER PRODUCT LIST");
                outlinks = getOutlinksFromProductList("http://llbstage.skavaone.com/skavastream/core/v1/llbean/", json, outlinks);
            }

            System.out.println(type + " +++++_____________________________________");

        } catch(Exception e) {
            System.out.println("------- FOUND EXCEPTION -----------");
            System.out.println(content);
            System.out.println("------- END FOUND EXCEPTION -----------");
            System.out.println(e);
        }

        Outlink[] outs = new Outlink[0];

        try {
            outs = outlinks.toArray(new Outlink[outlinks.size()]);
        } catch (Exception e) {
            // do nothing..
            System.out.println(e);
            e.printStackTrace();
        }
        /*
        try {
        outlinks[0] = new Outlink("http://www.cnn.com", "CNN");
        } catch (Exception e) {
            // do nothing...
        }
        */


        ParseStatus status = new ParseStatus();
        status.setMajorCode(ParseStatusCodes.SUCCESS);
        status.setMinorCode(ParseStatusCodes.SUCCESS);
        System.out.println("Parse Status" + status);

        Parse parse = new Parse(content, name, outs, status);

        return parse;


    }

    public Vector getOutlinksFromCategory(String urlBase, JSONObject json, Vector outlinks) {
            System.out.println("PARSING CATEGORY");
            if (json.containsKey("children")) {
                System.out.println("FOUND CHILDREN");
                JSONObject children = (JSONObject)json.get("children");

                if (children.containsKey("category")) {
                    System.out.println("FOUND CATEGORY");
                    JSONArray categories = (JSONArray)children.get("category");
                    for (int i = 0; i < categories.size(); i++) {
                        JSONObject category = (JSONObject) categories.get(i);
                        try {
                            System.out.println(urlBase + "category/" + (String)category.get("id"));
                            outlinks.add(new Outlink(urlBase + "category/" + (String)category.get("id"), (String)category.get("name")));
                        } catch (java.net.MalformedURLException e) {
                            // do nothing
                            System.out.println("FOUND ERROR");
                        }
                    }

                }
            }

            // add the product list url...
            try {
                outlinks.add(new Outlink(urlBase + "productlist/" + (String)json.get("id"), (String)json.get("name") + " ProductList"));
            } catch (java.net.MalformedURLException e) {
                System.out.println("Product List Link Error");
            }


            return outlinks;
        }

    public Vector getOutlinksFromProductList(String urlBase, JSONObject json, Vector outlinks) {
        System.out.println("PARSING PRODUCT LIST");
        if (json.containsKey("children")) {
            System.out.println("FOUND CHILDREN");
            JSONObject children = (JSONObject)json.get("children");

            if (children.containsKey("products")) {
                System.out.println("FOUND PRODUCTS");
                JSONArray products = (JSONArray)children.get("products");
                for (int i = 0; i < products.size(); i++) {
                    JSONObject product = (JSONObject) products.get(i);
                    try {
                        System.out.println(urlBase + "product/" + (String)product.get("id"));
                        outlinks.add(new Outlink(urlBase + "product/" + (String)product.get("id"), (String)product.get("name")));
                    } catch (java.net.MalformedURLException e) {
                        // do nothing
                        System.out.println("FOUND ERROR");
                    }
                }

            }
        }

        return outlinks;
    }

    @Override
    public Collection<WebPage.Field> getFields() {
        return null;
    }

    public void setConf(Configuration conf) {
        this.conf = conf;
    }

    public Configuration getConf() {
        return this.conf;
    }



}