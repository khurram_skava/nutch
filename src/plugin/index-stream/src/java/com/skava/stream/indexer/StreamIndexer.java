

package com.skava.stream.indexer;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.indexer.IndexingException;
import org.apache.nutch.indexer.IndexingFilter;
import org.apache.nutch.indexer.NutchDocument;
import org.apache.nutch.metadata.Nutch;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.util.Bytes;
import org.apache.nutch.util.TableUtil;
import org.apache.solr.common.util.DateUtil;

/** Adds basic searchable fields to a document. The fields are:
 * host - add host as un-stored, indexed and tokenized
 * site - add site as un-stored, indexed and un-tokenized
 * url - url is both stored and indexed, so it's both searchable and returned.
 * This is also a required field.
 * orig - also store original url as both stored and indexed
 * content - content is indexed, so that it's searchable, but not stored in index
 * title - title is stored and indexed
 * cache - add cached content/summary display policy, if available
 * tstamp - add timestamp when fetched, for deduplication
 */
public class StreamIndexer implements IndexingFilter {
  public static final Logger LOG = LoggerFactory.getLogger(StreamIndexer.class);

  private int MAX_TITLE_LENGTH;
  private Configuration conf;

  private static final Collection<WebPage.Field> FIELDS = new HashSet<WebPage.Field>();

  static {
    FIELDS.add(WebPage.Field.TITLE);
    FIELDS.add(WebPage.Field.TEXT);
    FIELDS.add(WebPage.Field.FETCH_TIME);
  }


  public NutchDocument filter(NutchDocument doc, String url, WebPage page)
      throws IndexingException {

    System.out.println("++++++++++ IN Stream Indexing FILTER +++++++++++++++");



    return doc;
  }

  public void addIndexBackendOptions(Configuration conf) {
  }

  /**
   * Set the {@link Configuration} object
   */
  public void setConf(Configuration conf) {
    this.conf = conf;
    this.MAX_TITLE_LENGTH = conf.getInt("indexer.max.title.length", 100);
    LOG.info("Maximum title length for indexing set to: " + this.MAX_TITLE_LENGTH);
  }

  /**
   * Get the {@link Configuration} object
   */
  public Configuration getConf() {
    return this.conf;
  }

  /**
   * Gets all the fields for a given {@link WebPage}
   * Many datastores need to setup the mapreduce job by specifying the fields
   * needed. All extensions that work on WebPage are able to specify what fields
   * they need.
   */
  @Override
  public Collection<WebPage.Field> getFields() {
    return FIELDS;
  }

}
