
package com.skava.cloudsearch;



import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.nutch.indexer.NutchDocument;
import org.apache.nutch.indexer.NutchIndexWriter;
import org.apache.nutch.util.TableUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CloudSearchWriter implements NutchIndexWriter {

  public static Logger LOG = LoggerFactory.getLogger(CloudSearchWriter.class);

  private static final int DEFAULT_MAX_BULK_DOCS = 500;
  private static final int DEFAULT_MAX_BULK_LENGTH = 5001001; // ~5MB



  @Override
  public void write(NutchDocument doc) throws IOException {

  }



  @Override
  public void close() throws IOException {

  }

  @Override
  public void open(TaskAttemptContext job) throws IOException {

  }

}
