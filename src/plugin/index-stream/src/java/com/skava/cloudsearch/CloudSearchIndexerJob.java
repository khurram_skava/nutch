package com.skava.cloudsearch;

import java.util.Map;

import org.apache.hadoop.util.ToolRunner;
import org.apache.nutch.indexer.IndexerJob;
import org.apache.nutch.indexer.NutchIndexWriterFactory;
import org.apache.nutch.metadata.Nutch;
import org.apache.nutch.util.NutchConfiguration;
import org.apache.nutch.util.ToolUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CloudSearchIndexerJob extends IndexerJob {

  public static Logger LOG = LoggerFactory.getLogger(CloudSearchIndexerJob.class);

  @Override
  public Map<String,Object> run(Map<String,Object> args) throws Exception {
    LOG.info("Starting");

    NutchIndexWriterFactory.addClassToConf(getConf(), CloudSearchWriter.class);
    String batchId = (String)args.get(Nutch.ARG_BATCH);
    String clusterName = (String)args.get(CloudSearchConstants.CLUSTER);

    getConf().set(CloudSearchConstants.CLUSTER, clusterName);

    currentJob = createIndexJob(getConf(), "elastic-index [" + clusterName + "]", batchId);

    currentJob.waitForCompletion(true);
    ToolUtil.recordJobStatus(null, currentJob, results);

    LOG.info("Done");
    return results;
  }

  public void indexCloudSearch(String clusterName, String batchId) throws Exception {
    run(ToolUtil.toArgMap(CloudSearchConstants.CLUSTER, clusterName,
                          Nutch.ARG_BATCH, batchId));
  }

  public int run(String[] args) throws Exception {
    if (args.length == 2) {
      //ok
    } else if (args.length == 4 && "-crawlId".equals(args[2])) {
      getConf().set(Nutch.CRAWL_ID_KEY, args[3]);
    } else {
      System.err.println("Usage: <elastic cluster name> (<batchId> | -all | -reindex) [-crawlId <id>]");
      return -1;
    }
    indexCloudSearch(args[0], args[1]);
    return 0;
  }

  public static void main(String[] args) throws Exception {
    int res = ToolRunner.run(NutchConfiguration.create(), new CloudSearchIndexerJob(), args);
    System.exit(res);
  }
}
